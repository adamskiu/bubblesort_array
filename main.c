#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include "bubblesort.h"
#define SIZE 20

int main(int argc, char **argv)
{
	int i;
	srand(time(0));
	int array[SIZE];

	for (i = 0; i < SIZE; i++){
		array[i] = rand() % 100;
		printf("%d\n", array[i]);
	}
	
	printf("\n");
	bubblesort(array, SIZE);
	for (i = 0; i < SIZE; i++)
		printf("%d\n", array[i]);
		
}
