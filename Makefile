TARGET = sort
CC = gcc
SOURCES = bubblesort.c main.c
CFLAGS = -Wall -Werror -g
LDFLAGS = 
INCLUDES = bubblesort.h

all:	$(TARGET)

$(TARGET) : $(INCLUDES) $(SOURCES)
	$(CC) $(CFLAGS) $(LDFLAGS) $(SOURCES) -o $(TARGET)

clean:
	rm $(TARGET)
