#include "bubblesort.h"

void bubblesort(int *toSort, int size)
{
	int i;
	int temp;
	do{
		for(i = 0; i < size - 1; i++){
			if( toSort[i] > toSort[i+1] ){
				temp = toSort[i];
				toSort[i] = toSort[i+1];
				toSort[i+1] = temp;
			}
		}
		size--;
	}while(size > 1);
}
